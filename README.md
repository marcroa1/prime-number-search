# Getting Started

### How to run it
To run the application just run the following command in the project library:
```
gradle bootRun
```

### Accessing REST endpoints in the browser
There is a swagger UI for the available REST endpoint, which can be accessed [here](http://localhost:8080/swagger-ui/index.html)

* [POST - Start prime number calculation](http://localhost:8080/prime/start-calculating)
* [POST - Stop prime number calculation](http://localhost:8080/prime/stop-calculating)
* [GET - Get already calculated prime numbers from 0 to 10](http://localhost:8080/prime/get/0/10)

