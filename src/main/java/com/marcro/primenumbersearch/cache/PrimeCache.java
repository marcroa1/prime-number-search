package com.marcro.primenumbersearch.cache;

import com.marcro.primenumbersearch.exception.NotYetCalculatedException;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

public class PrimeCache {
    private final ConcurrentSkipListSet<Long> cache = new ConcurrentSkipListSet<>();
    private static final PrimeCache instance = new PrimeCache();

    private PrimeCache() {
    }

    // VisibleForTesting
    protected PrimeCache(Collection<Long> primes) {
        primes.stream().sorted(Long::compareTo).forEach(cache::add);
    }

    public static PrimeCache getInstance() {
        return instance;
    }

    public Set<Long> get(Long from, Long to) throws NotYetCalculatedException {
        Long last = getLast();
        if (last == null || last < to) {
            throw new NotYetCalculatedException();
        }
        return cache.subSet(from, false, to, false);
    }

    public void add(long prime) {
        cache.add(prime);
    }

    public Long getLast() {
        try {
            return cache.last();
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}
