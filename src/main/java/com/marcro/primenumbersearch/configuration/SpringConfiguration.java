package com.marcro.primenumbersearch.configuration;

import com.marcro.primenumbersearch.runner.PrimeRunnerService;
import com.marcro.primenumbersearch.validate.RestValidator;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {

    @Value("${max.thread.count}")
    private String maxThreadCount;
    @Value("${max.prime.response}")
    private String maxResponseCount;

    @Bean
    public OpenAPI springOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("Prime Number Search")
                        .description("Searching for prime numbers")
                        .version("v0.0.1"));
    }

    @Bean
    public PrimeRunnerService primeRunnerService() {
        return new PrimeRunnerService();
    }

    @Bean
    public RestValidator restValidator() {
        return new RestValidator(Integer.parseInt(maxThreadCount), Integer.parseInt(maxResponseCount));
    }
}
