package com.marcro.primenumbersearch.runner;

import com.marcro.primenumbersearch.cache.PrimeCache;
import com.marcro.primenumbersearch.exception.AlreadyRunningException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static com.marcro.primenumbersearch.calculator.PrimeCalculator.isPrime;

public class PrimeRunnerService {
    Logger log = LoggerFactory.getLogger(PrimeRunnerService.class);
    private final AtomicBoolean isRunning = new AtomicBoolean(false);
    private final AtomicBoolean canRun = new AtomicBoolean(true);

    public synchronized boolean stopProcess() {
        canRun.compareAndSet(true, false);
        return !runCondition();
    }

    public synchronized boolean startProcess(int threadCount) throws AlreadyRunningException {
        if (isRunning.get()) {
            throw new AlreadyRunningException();
        }
        isRunning.compareAndSet(false,true);

        AtomicLong startNumber = getLastCachedPrime();
        int queueSize = threadCount * 2;

        ThreadPoolExecutor threadPoolExecutor = newFixedAndLimitedThreadPoolExecutor(threadCount, queueSize);

        new Thread(() -> {
            try {
                while (runCondition()) {
                    if (canAddIntoThreadPool(threadPoolExecutor, queueSize)) {
                        threadPoolExecutor.execute(cacheNumberIfPrime(startNumber.getAndIncrement()));
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage());
                log.error(Arrays.toString(e.getStackTrace()));
            } finally {
                threadPoolExecutor.shutdown(); // Let Runnable-s in the ThreadPoolQueue finish the job.
                refreshRunCondition();
            }
        }).start();

        return runCondition();
    }

    private boolean runCondition() {
        // Also use isRunning to ensure single the process is running in single instance
        return canRun.get() && isRunning.get();
    }

    private void refreshRunCondition() {
        isRunning.compareAndSet(true, false);
        canRun.compareAndSet(false, true);
    }

    private static boolean canAddIntoThreadPool(ThreadPoolExecutor threadPoolExecutor, int queueSize) {
        return threadPoolExecutor.getQueue().size() < queueSize;
    }

    private static ThreadPoolExecutor newFixedAndLimitedThreadPoolExecutor(int threadCount, int queueLimit) {
        return new ThreadPoolExecutor(threadCount, threadCount, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(queueLimit));
    }

    private static AtomicLong getLastCachedPrime() {
        return new AtomicLong(
                Objects.requireNonNullElse(PrimeCache.getInstance().getLast(), 2L)
        );
    }

    private Runnable cacheNumberIfPrime(Long number) {
        return () -> {
            if (isPrime(number)) {
                log.info(String.valueOf(number));
                PrimeCache.getInstance().add(number);
            }
        };
    }
}
