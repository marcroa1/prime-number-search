package com.marcro.primenumbersearch.validate;

import com.marcro.primenumbersearch.exception.RangeException;
import com.marcro.primenumbersearch.exception.InvalidThreadCountException;

import javax.naming.SizeLimitExceededException;
import java.util.Collection;

public class RestValidator {
    private final Integer maxThreadCount;
    private final Integer maxResponseCount;

    public RestValidator(Integer maxThreadCount, Integer maxResponseCount) {
        this.maxThreadCount = maxThreadCount;
        this.maxResponseCount = maxResponseCount;
    }

    public void validateThreads(int threadCount) throws InvalidThreadCountException {
        if (threadCount > maxThreadCount) {
            throw new InvalidThreadCountException(threadCount + " is larger, than the configured maximum thread count (" + maxThreadCount + ")");
        }
        if (threadCount < 1) {
            throw new InvalidThreadCountException("At least one thread is necessary");
        }
    }

    public void validateResponseCount(Collection<?> collection) throws SizeLimitExceededException {
        int collectionSize = collection.size();
        if (collectionSize > maxResponseCount) {
            throw new SizeLimitExceededException("Collection size (" + collectionSize + ") exceeds the max limit (" + maxResponseCount + ")");
        }
    }

    public void validateFromTo(String from, String to) throws NumberFormatException, RangeException {
        long fromNumber = parseNumber(from);
        long toNumber = parseNumber(to);

        if (fromNumber >= toNumber) {
            throw new RangeException("'TO' number has to be grater than 'FROM' number");
        }

    }

    private static long parseNumber(String number) {
        try {
            return Long.parseLong(number);

        } catch (NumberFormatException e) {
            throw new NumberFormatException(number + " is not a number");
        }
    }
}
