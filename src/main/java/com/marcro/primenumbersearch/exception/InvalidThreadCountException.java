package com.marcro.primenumbersearch.exception;

public class InvalidThreadCountException extends Exception {
    public InvalidThreadCountException(String message) {
        super(message);
    }
}
