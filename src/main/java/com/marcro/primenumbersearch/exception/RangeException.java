package com.marcro.primenumbersearch.exception;

public class RangeException extends Exception {
    public RangeException(String message) {
        super(message);
    }
}
