package com.marcro.primenumbersearch.rest;

import com.marcro.primenumbersearch.cache.PrimeCache;
import com.marcro.primenumbersearch.exception.AlreadyRunningException;
import com.marcro.primenumbersearch.exception.NotYetCalculatedException;
import com.marcro.primenumbersearch.exception.RangeException;
import com.marcro.primenumbersearch.exception.InvalidThreadCountException;
import com.marcro.primenumbersearch.runner.PrimeRunnerService;
import com.marcro.primenumbersearch.validate.RestValidator;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.SizeLimitExceededException;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("prime")
public class PrimeController {

    public static final int SINGLE_THREAD = 1;
    private final PrimeRunnerService primeRunnerService;
    private final RestValidator validator;

    public PrimeController(PrimeRunnerService primeRunnerService, RestValidator restValidator) {
        this.primeRunnerService = primeRunnerService;
        this.validator = restValidator;
    }

    @GetMapping(value = "/get/{from}/{to}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get prime numbers", description = "You can request already calculated prime numbers within the given from-to range")
    public ResponseEntity<Map<String, Object>> getPrime(@Parameter(description = "From number (exclusive)") @PathVariable String from,
                                                        @Parameter(description = "To number (exclusive)") @PathVariable String to) {
        try {
            validator.validateFromTo(from, to);

            Set<Long> primes = PrimeCache.getInstance().get(
                    Long.parseLong(from),
                    Long.parseLong(to)
            );

            validator.validateResponseCount(primes);

            return Response.PRIMES.custom(primes);
        } catch (NumberFormatException | RangeException e) {
            return Response.BAD_REQUEST.custom(e.getMessage());
        } catch (SizeLimitExceededException e) {
            return Response.PAYLOAD_TOO_LARGE.custom(e.getMessage());
        } catch (NotYetCalculatedException e) {
            return Response.NOT_CALCULATED.send();
        }
    }

    @PostMapping(value = "/start-calculating", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Start the prime number search", description = "Start searching the prime numbers from 2, with the given thread count")
    public ResponseEntity<Map<String, Object>> starProcess(@Parameter(description = "Number of threads to calculate (max 5)") @RequestParam String threads) {

        try {
            int threadCount = isNotPresent(threads) ? SINGLE_THREAD : Integer.parseInt(threads);
            validator.validateThreads(threadCount);

            if (primeRunnerService.startProcess(threadCount)) {
                return Response.PROCESS_STARTED.send();
            } else {
                return Response.PROCESS_NOT_STARTED.send();
            }

        } catch (NumberFormatException | InvalidThreadCountException e) {
            return Response.BAD_REQUEST.custom(e.getMessage());
        } catch (AlreadyRunningException e) {
            return Response.ALREADY_RUNNING.send();
        }
    }

    private static boolean isNotPresent(String threads) {
        return threads == null || threads.isBlank();
    }

    @PostMapping(value = "/stop-calculating", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Stop the running process gracefully", description = "Stop the process, but wait already running threads")
    public ResponseEntity<Map<String, Object>> stopProcess() {

        if (primeRunnerService.stopProcess()) {
            return Response.PROCESS_STOPPED.send();
        } else {
            return Response.PROCESS_NOT_STOPPED.send();
        }
    }
}
