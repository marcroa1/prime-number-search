package com.marcro.primenumbersearch.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public enum Response {
    PROCESS_STOPPED("message", "process stopped", HttpStatus.OK),
    PROCESS_STARTED("message", "process started", HttpStatus.OK),
    PROCESS_NOT_STARTED("error", "Could not start process", HttpStatus.INTERNAL_SERVER_ERROR),
    PROCESS_NOT_STOPPED("error", "Could not stop process", HttpStatus.INTERNAL_SERVER_ERROR),
    ALREADY_RUNNING("error", "Process is already running", HttpStatus.TOO_MANY_REQUESTS),
    PRIMES("primes", "", HttpStatus.OK),
    BAD_REQUEST("error", "", HttpStatus.BAD_REQUEST),
    PAYLOAD_TOO_LARGE("error", "", HttpStatus.PAYLOAD_TOO_LARGE),
    NOT_CALCULATED("message", "The requested range is not yet calculated", HttpStatus.OK);

    private final String attribute;
    private final String message;
    private final HttpStatus status;

    Response(String attribute, String message, HttpStatus status) {
        this.attribute = attribute;
        this.message = message;
        this.status = status;
    }

    public ResponseEntity<Map<String, Object>> send() {
        return new ResponseEntity<>(Map.of(attribute, message), status);
    }

    public ResponseEntity<Map<String, Object>> custom(Object value) {
        return new ResponseEntity<>(Map.of(attribute, value), status);
    }


}
