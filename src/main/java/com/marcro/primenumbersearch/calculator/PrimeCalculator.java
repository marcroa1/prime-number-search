package com.marcro.primenumbersearch.calculator;

public class PrimeCalculator {
    public static boolean isPrime(Long number) {
        if (number < 2L) {
            // 0 and 1 is not a prime number
            return false;
        }
        // from 2 to half of the number, as a number can't be divisible by more than its half
        for (int i = 2; i <= number / 2; ++i) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
