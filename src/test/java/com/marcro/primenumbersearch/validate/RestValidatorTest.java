package com.marcro.primenumbersearch.validate;

import com.marcro.primenumbersearch.exception.InvalidThreadCountException;
import com.marcro.primenumbersearch.exception.RangeException;
import org.junit.jupiter.api.Test;

import javax.naming.SizeLimitExceededException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class RestValidatorTest {

    RestValidator underTest = new RestValidator(5, 10);

    @Test
    public void testThreadValidation_zeroThread() {
        InvalidThreadCountException exception = assertThrows(InvalidThreadCountException.class, () -> underTest.validateThreads(0));
        assertEquals("At least one thread is necessary", exception.getMessage());
    }

    @Test
    public void testThreadValidation_tooMuchThread() {
        InvalidThreadCountException exception = assertThrows(InvalidThreadCountException.class, () -> underTest.validateThreads(6));
        assertEquals("6 is larger, than the configured maximum thread count (5)", exception.getMessage());
    }

    @Test
    public void testThreadValidation() {
        assertDoesNotThrow(() -> underTest.validateThreads(4));
    }

    @Test
    public void testResponseCountValidation_sizeLimit() {
        SizeLimitExceededException exception = assertThrows(SizeLimitExceededException.class, () -> underTest.validateResponseCount(Set.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)));
        assertEquals("Collection size (12) exceeds the max limit (10)", exception.getMessage());
    }

    @Test
    public void testResponseCountValidation() {
        assertDoesNotThrow(() -> underTest.validateResponseCount(Set.of(1, 2, 3, 4)));
    }

    @Test
    public void testFromToValidation_wrongNumberFormat() {
        NumberFormatException fromException = assertThrows(NumberFormatException.class, () -> underTest.validateFromTo("sphere", "5"));
        NumberFormatException toException = assertThrows(NumberFormatException.class, () -> underTest.validateFromTo("3", "Lemon"));

        assertEquals("sphere is not a number", fromException.getMessage());
        assertEquals("Lemon is not a number", toException.getMessage());
    }

    @Test
    public void testFromToValidation_invalidRange() {
        RangeException exception = assertThrows(RangeException.class, () -> underTest.validateFromTo("40", "20"));
        assertEquals("'TO' number has to be grater than 'FROM' number", exception.getMessage());

    }
}
