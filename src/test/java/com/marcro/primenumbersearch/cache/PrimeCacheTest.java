package com.marcro.primenumbersearch.cache;

import com.marcro.primenumbersearch.exception.NotYetCalculatedException;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class PrimeCacheTest {


    @Test
    public void testCacheCanRetrieveNumbersFromTo() throws NotYetCalculatedException {
        PrimeCache primeCache = new PrimeCache(List.of(2L,3L,5L,7L,11L));
        assertEquals(Set.of(2L, 3L, 5L, 7L), primeCache.get(1L, 11L));
    }

    @Test
    public void testCacheCanRetrieveNumbersFromTo_differentOrder() throws NotYetCalculatedException {
        PrimeCache primeCache = new PrimeCache(List.of(5L,7L,3L,11L,2L));
        assertEquals(Set.of(3L, 5L), primeCache.get(2L, 7L));
    }

    @Test
    public void testCanAddToCache() throws NotYetCalculatedException {
        PrimeCache primeCache = PrimeCache.getInstance();
        assertThrows(NotYetCalculatedException.class, () -> primeCache.get(1L, 10L));

        primeCache.add(5L);
        primeCache.add(7L);
        primeCache.add(11L);

        assertEquals(Set.of(5L, 7L), primeCache.get(1L, 11L));
    }

    @Test
    public void testCacheCanWorkMultiThreaded() throws InterruptedException, NotYetCalculatedException {
        PrimeCache primeCache = PrimeCache.getInstance();
        primeCache.add(2L);
        primeCache.add(3L);
        primeCache.add(5L);
        assertDoesNotThrow(() -> primeCache.get(2L, 5L));
        Set<Long> input = Set.of(7L, 11L, 13L, 17L, 19L, 23L);
        Set<Long> expected = Set.of(2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L);
        new Thread(() -> input
                .parallelStream()
                .forEach(primeCache::add)
        ).start();
        new Thread(() -> List.of(1, 2, 3, 4, 5, 6, 7, 8)
                    .parallelStream()
                    .forEach(i -> {
                        Set<Long> primes = assertDoesNotThrow(() -> primeCache.get(2L, 5L));
                        assertEquals(Set.of(3L), primes);
                    })
        ).start();

        Thread.sleep(1000);
        assertEquals(expected, primeCache.get(1L, 23L));
    }

    @Test
    public void testCacheGetLastPrime() {
        PrimeCache primeCache = new PrimeCache(Set.of());
        assertNull(primeCache.getLast());
        primeCache.add(3L);
        primeCache.add(11L);
        primeCache.add(7L);
        assertEquals(11L, primeCache.getLast());
    }
}
