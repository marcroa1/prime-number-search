package com.marcro.primenumbersearch.calculator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PrimeCalculatorTest {

    @Test
    public void testIsPrime_Positive() {
        assertTrue(PrimeCalculator.isPrime(2L));
        assertTrue(PrimeCalculator.isPrime(3L));
        assertTrue(PrimeCalculator.isPrime(5L));
        assertTrue(PrimeCalculator.isPrime(7L));
        assertTrue(PrimeCalculator.isPrime(11L));
        assertTrue(PrimeCalculator.isPrime(13L));
        assertTrue(PrimeCalculator.isPrime(17L));
        assertTrue(PrimeCalculator.isPrime(23L));
    }

    @Test
    public void testIsPrime_Negative() {
        assertFalse(PrimeCalculator.isPrime(0L));
        assertFalse(PrimeCalculator.isPrime(1L));
        assertFalse(PrimeCalculator.isPrime(4L));
        assertFalse(PrimeCalculator.isPrime(6L));
        assertFalse(PrimeCalculator.isPrime(8L));
        assertFalse(PrimeCalculator.isPrime(9L));
        assertFalse(PrimeCalculator.isPrime(10L));
        assertFalse(PrimeCalculator.isPrime(12L));
        assertFalse(PrimeCalculator.isPrime(14L));
        assertFalse(PrimeCalculator.isPrime(15L));
        assertFalse(PrimeCalculator.isPrime(16L));
        assertFalse(PrimeCalculator.isPrime(18L));
        assertFalse(PrimeCalculator.isPrime(20L));
    }
}
